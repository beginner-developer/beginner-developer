package com.greedy;

import java.util.Scanner;

public class Hard extends Question {

	private int point;
	private int answer;
	private int random;
	Scanner sc = new Scanner(System.in);

	// Question에 맞는 정답인경우 point증가,틀린경우 정답과 설명이 나온다.
	@Override
	public void answer() {
		switch (random) {
		case 1: {
			if (answer == 2) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요.. \n정답은 2번 ");
				System.out.println("private 및 static 키워드는 클래스 선언 시 사용할 수 없다");
			}
			break;
		}
		case 2: {
			if (answer == 1) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..\n정답은 1번");
				System.out.println("Fruits 클래스의 getName() 메소드는 final 키워드를 포함하고 있으므로 오버로딩(overload) 할 수 없다.");
			}
			break;
		}
		case 3: {
			if (answer == 4) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..\n정답은 4번");
				System.out.println("레퍼런스 변수에 대한 배열로, 객체생성시 객체배열 주소를 레퍼런스 배열에 대입하여 2차원 배열과 비슷하다.");
			}

			break;
		}
		case 4: {
			if (answer == 3) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..\n정답은 3번");
				System.out.println("레퍼런스 변수로 생성자 실행시 자동으로 주소를 전달 받기 때문에 동적 메소드이다.");
			}

			break;
		}
		case 5: {
			if (answer == 4) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..\n정답은 4번");
				System.out.println("doStuff메소드는 메인 메소드에서 변수를 받아서 다시 메인 메소드로 반환하지 않아 메인 메소드 변수값은 변함없음");
				System.out.println("doStuff 변수는 x++로 출력문 출력 후 x값을 증가시키는 연산자로 출력은 메인 메소드에서 받은 값으로 출력된다.");
			}
			break;
		}
		default:
			break;
		}
		point();
		{
		}
	}

	// 단계에 맞는 문제 랜덤으로 출력 및 정답값 반환
	@Override
	public void question() {
		random = (int) ((Math.random() * 5) + 1);
		System.out.println();
		switch (random) {
		case 1: {

			System.out.println("고급 문제) 다음 중 밑줄에 들어갈 수 없는 키워드는?");
			System.out.println("	_____ class ATestClass { \n		public static void main(String[] arg) {");
			System.out.println("				}");
			System.out.println("			}");
			System.out.println("1. public\n2. private\n3. abstract\n4. final");
			System.out.print("정답 : ");
			answer = sc.nextInt();
		}
			break;
		case 2: {
			System.out.println("고급 문제) 다음을 컴파일 하면 결과값으로 올바른 것은?");
			System.out
					.println("public class OverloadTest {\r\n" + "		public static void main(String[] argv) {\r\n"
							+ "			new OverloadTest().test();\r\n" + "		}\r\n" + "\r\n" + "");
			System.out.println("		private void test() {\r\n" + "			Fruits fruits = new Apple();\r\n"
					+ "			System.out.println(fruits.getName());\r\n" + "		}\r\n" + "\r\n" + "");
			System.out.println("		public class Fruits {\r\n" + "			private String name;\r\n" + "\r\n"
					+ "			public Fruits() {\r\n" + "				this.name = \"unknown\";\r\n"
					+ "			}\r\n" + "\r\n" + "			final public String getName() {\r\n"
					+ "				return name;\r\n" + "			}\r\n" + "		}\r\n" + "\r\n" + "");
			System.out.println(
					"		public class Apple extends Fruits {\r\n" + "			public String getName() {\r\n"
							+ "				return \"Apple\";\r\n" + "			}\r\n" + "		}\r\n" + "	}");
			System.out.println("1. 컴파일 오류\n2. unknown\n3. Null\n4. Apple");
			System.out.print("정답 : ");
			answer = sc.nextInt();
		}
			break;
		case 3: {
			System.out.println("고급 문제) 객체 배열에 대한 설명으로 옳은 것은?");
			System.out.println("1. 메모리에 객체들을 연속 나열한다.");
			System.out.println("2. 한번의 할당으로 지정한 갯수만큼 자동 객체가 생성된다.");
			System.out.println("3. 생성된 객체의 인스턴스 변수에는 쓰레기 값이 들어간다.");
			System.out.println("4. 객체 배열은 기본자료형 2차원배열과 유사한 레퍼런스 배열이다.");
			System.out.print("정답 : ");
			answer = sc.nextInt();

		}
			break;
		case 4: {
			System.out.println("고급 문제) this 레퍼런스의 설명으로 틀린 것은? ");
			System.out.println("1. 메소드와 생성자 실행시 자동으로 주소를 전달받는다. ");
			System.out.println("2. 생성자와 일반 메소드 안에 기본으로 존재하는 레퍼런스 변수이다. ");
			System.out.println("3. 정적 메소드에서도 사용할 수 있다. ");
			System.out.println("4. 전달받은 주소로 인스턴스 변수에 접근하는 레퍼런스이다. ");
			System.out.print("정답 : ");
			answer = sc.nextInt();
		}
			break;
		case 5: {
			System.out.println("고급 문제) 아래 소스 코드의 컴파일 및 실행 결과는? ");
			System.out.println("public class Pass {\r\n" + "void doStuff(int x) {\r\n"
					+ "System.out.print(\"doStuff x = \" + (x++));\r\n" + "}\r\n"
					+ "public static void main(String [] args) {\r\n" + "int x = 5;\r\n" + "Pass p = new Pass();\r\n"
					+ "p.doStuff(x);\r\n" + "System.out.print(\"main x = \" + x);\r\n" + "}\r\n" + "}");
			System.out.println("Compilation fails.");
			System.out.println("An exception is thrown at runtime.");
			System.out.println("1. doStuff x = 6 main x = 6");
			System.out.println("2. doStuff x = 5 main x = 5");
			System.out.println("3. doStuff x = 5 main x = 6");
			System.out.println("4. doStuff x = 6 main x = 5");
			System.out.print("정답 : ");
			answer = sc.nextInt();
		}
			break;
		default:
			break;
		}
		answer();
	}

	// 문제를 풀어 정답인 경우 point 점수가 쌓인다.쌓인 포인트 점수로 등급을 나눈다
	@Override
	public void point() {
		if (point >= 100) {
			super.setHardPoint(3);
		} else if (point >= 60) {
			super.setHardPoint(2);
		} else if (point >= 30) {
			super.setHardPoint(1);
		} else if (point == 0) {
			super.setHardPoint(0);
		}

	}

	// 포인트 점수로 나뉜 등급에 따라 출력되는 출력문
	@Override
	public void level() {
		switch (super.getHardPoint()) {
		case 0: {
			System.out.println("고급 지식을 공부하고 오세요 ");
		}
			break;
		case 1: {
			System.out.println("고급 지식을 어느정도 습득했습니다. ");
		}
			break;
		case 2: {
			System.out.println("고급 지식 마스터에 근접했습니다.");
		}
			break;
		case 3: {
			System.out.println("고급 지식을 마스터 했습니다.");
		}
			break;
		}

	}

}
