package com.greedy;

import java.util.Scanner;

public class Normal extends Question {

	private int point;
	private int answer;
	private int random;
	Scanner sc = new Scanner(System.in);

	// Question에 맞는 정답인경우 point증가,틀린경우 정답과 설명이 나온다.
	@Override
	public void answer() {
		switch (random) {
		case 1: {
			if (answer == 3) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요.. \n정답은 3번 ");
				System.out.println("boolean 타입의 변수 범위는 true,false 이다. ");
			}
			break;
		}
		case 2: {
			if (answer == 4) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요.. \n정답은 4번 ");
				System.out.println("상속불가하게 만든 final클래스나, 최상위 클래스인 Object는 불가능하다.");
			}
			break;
		}
		case 3: {
			if (answer == 2) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요.. \n정답은 2번 ");
				System.out.println("set과 List는 요소값으로 구성되지만 Map은 Key+value을 하나의 Entry로 구성하고 있다.");
			}

			break;
		}
		case 4: {
			if (answer == 3) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..  \n정답은 3번 ");
				System.out.println("9를 0으로 나누는것은 문법적인 오류(컴파일오류)는 없으나, 실행시 오류(runtime 오류)가 발생한다.");

			}

			break;
		}
		case 5: {
			if (answer == 4) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..  \n정답은 4번 ");
				System.out.println("일반필드나 메소드 선언이 가능한것은 interface가 아니라 추상클래스이다.");
			}
			break;
		}
		default:
			break;
		}
		point();

	}

	// 단계에 맞는 문제 랜덤으로 출력 및 정답값 반환
	@Override
	public void question() {
		random = (int) ((Math.random() * 5) + 1);
		System.out.println();
		switch (random) {
		case 1:
			System.out.println("중급 문제) 자바 데이터 타입에 대한 설명 중 틀린 것을 고르시오");
			System.out.println("1. String 타입은 참조형이다.");
			System.out.println("2. char 타입 변수는 2 byte의 메모리 공간을 사용한다.");
			System.out.println("3. boolean 타입 변수의 범위는 -128~127까지이다. ");
			System.out.println("4. int 타입 변수는 4 byte의 메모리 공간을 사용한다.");
			System.out.print("정답 : ");
			answer = sc.nextInt();
			break;
		case 2:
			System.out.println("중급 문제) 다음 자바 언어에서의 객체지향 기법에 대한 설명 중 틀린 것을 고르시오.");
			System.out.println("1. 클래스는 복수의 인터페이스를 구현(implements)할 수 있다.");
			System.out.println("2. 클래스는 복수의 부모 클래스(super class)로부터 상속(inherit) 받을 수 없다");
			System.out.println("3. 인터페이스는 또 다른 인터페이스로부터 상속 받을 수 있다");
			System.out.println("4. 모든 클래스는 상속을 통한 확장(extend)이 가능하다.");
			System.out.print("정답 : ");
			answer = sc.nextInt();
			break;
		case 3:
			System.out.println("중급 문제) Collection 중 key 값과 value 값으로 구성되어 있는 것은?");
			System.out.println("1. Collection   2. Map   3. List   4. Set");
			System.out.print("정답 : ");
			answer = sc.nextInt();
			break;
		case 4:
			System.out.println("중급 문제) 다음 코드 및 예측 결과 중 틀린 해석을 고르시오. ");
			System.out.println("1. int a = 3.5; // 컴파일 오류가 발생한다.");
			System.out.println("2. int a1 = 5; double a2 = (float)a1; // 정상 동작한다.");
			System.out.println("3. int a = 9 / 3; // 컴파일 오류가 발생한다.");
			System.out.println(
					"4. Integer a = new Integer(2); Integer b = new Integer(2); System.out.println( a == b ); // false를 출력한다");
			System.out.print("정답 : ");
			answer = sc.nextInt();
			break;
		case 5:
			System.out.println("중급 문제) interface에 대한 설명 중 잘못된 것은 ");
			System.out.println("1. interface 키워드를 이용하여  인터페이스를 선언한다.");
			System.out.println("2. 하나의 클래스는 다수의 인터페이스를 구현(implement)할 수 있다.");
			System.out.println("3. 인터페이스를 구현(implements)한 클래스는 오버라이딩이 강제화 된다.");
			System.out.println("4. interface에 일반 필드, 메소드를 선언 할 수 있다.");
			System.out.print("정답 : ");
			answer = sc.nextInt();
			break;
		}
		answer();

	}

	// 문제를 풀어 정답인 경우 point 점수가 쌓인다.쌓인 포인트 점수로 등급을 나눈다
	@Override
	public void point() {
		if (point >= 100) {
			super.setNormalPoint(3);
		} else if (point >= 60) {
			super.setNormalPoint(2);
		} else if (point >= 30) {
			super.setNormalPoint(1);
		} else if (point == 0) {
			super.setNormalPoint(0);
		}

	}

	// 포인트 점수로 나뉜 등급에 따라 출력되는 출력문
	@Override
	public void level() {
		switch (super.getNormalPoint()) {
		case 0: {
			System.out.println("중급 지식을 공부하고 오세요 ");
		}
			break;
		case 1: {
			System.out.println("중급 지식을 어느정도 습득했습니다. ");
		}
			break;
		case 2: {
			System.out.println("중급 지식 마스터에 근접했습니다.");
		}
			break;
		case 3: {
			System.out.println("중급 지식을 마스터 했습니다.");
		}
			break;
		}

	}

}
