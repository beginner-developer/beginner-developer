package com.greedy;

import java.util.Scanner;

public class Basic extends Question {
	private int point;
	private int answer;
	private int random;
	Scanner sc = new Scanner(System.in);

	// Question에 맞는 정답인경우 point증가,틀린경우 정답과 설명이 나온다.
	@Override
	public void answer() {
		switch (random) {
		case 1: {
			if (answer == 4) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..\n정답은 4번");
				System.out.println("String은 참조 자료형이다.");
			}
			break;
		}
		case 2: {
			if (answer == 3) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..\n정답은 3번");
				System.out.println("변수명은 예약어, 숫자시작 불가, 특수문자는 ' _ ', '$' 만 사용가능");
				System.out.println("대소문자를 구분하며, 동일범위내에 동일 변수명은 가질수 없다.");
			}
			break;
		}
		case 3: {
			if (answer == 1) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..\n정답은 1번");
				System.out.println("추상클래스는 추상메소드가 0개 이상으로 0개여도 된다.");
			}

			break;
		}
		case 4: {
			if (answer == 3) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..\n정답은 3번");
				System.out.println("접근제한자는 private -> defualt -> protecete -> public 순으로 접근허용범위가 넓다");
			}

			break;
		}
		case 5: {
			if (answer == 3) {
				System.out.println("정답입니다!");
				point += 10;
			} else {
				System.out.println("땡.. 더 공부하세요..\n정답은 3번");
				System.out.println("static은 클래스 로딩하면서 메모리가 할당되어 프로그램 종료시까지 영역의 고정된다.");
				System.out.println("static 메소드는 메소드가 static 일뿐 내부 변수는 지역변수로도 선언 할 수 있다.");

			}
			break;
		}
		default:
			break;
		}
		point();

	}

	// 단계에 맞는 문제 랜덤으로 출력 및 정답값 반환
	@Override
	public void question() {
		random = (int) ((Math.random() * 5) + 1);
		System.out.println();
		switch (random) {
		case 1:
			System.out.println("초급 문제) 다음중 기본형이 아닌것은?");
			System.out.println("1.int   2.Byte   3.double   4.String");
			System.out.print("정답 : ");
			answer = sc.nextInt();
			break;
		case 2:
			System.out.println("초급 문제) 다음중 변수명으로 사용 할 수 있는 것은?");
			System.out.println("1.7elven   2.Byte   3.$hort   4.num*");
			System.out.print("정답 : ");
			answer = sc.nextInt();
			break;
		case 3:
			System.out.println("초급 문제) 다음 설명 중 틀린 것을 고르시오.");
			System.out.println("1.추상 클래스는 하나 이상의 추상 메소드를 포함하고 있어야 한다.");
			System.out.println("2.추상 클래스는 객체를 생성할 수 없다");
			System.out.println("3.protected 메소드는 모든 하위 클래스에서 호출할 수 있다.");
			System.out.println("4.인터페이스를 구현한 클래스는 인터페이스에 포함된 모든 메소드를 구현하지 않아도 된다.");
			System.out.print("정답 : ");
			answer = sc.nextInt();
			break;
		case 4:
			System.out.println("초급 문제) 접근제한자의 범위가 가장 넓은 것은?");
			System.out.println("1.protected   2.private   3.public   4.default");
			System.out.print("정답 : ");
			answer = sc.nextInt();
			break;
		case 5:
			System.out.println("초급 문제) 다음 설명 중 틀린 것을 고르시오. ");
			System.out.println("1.멤버 변수와 메소드에 static을 지정할 수 있다.");
			System.out.println("2.static 형 변수는 클래스 로딩시에 메모리가 할당되어 프로그램 종료까지 그 영역이 고정된다.");
			System.out.println("3.static 메소드 안에 선언되는 변수들은 모두 static 변수가 된다.");
			System.out.println("4.static 메소드 안에서는 this 나 super 를 사용할 수 없다.");
			System.out.print("정답 : ");
			answer = sc.nextInt();
			break;
		}
		answer();

	}

	// 문제를 풀어 정답인 경우 point 점수가 쌓인다.쌓인 포인트 점수로 등급을 나눈다
	@Override
	public void point() {
		if (point >= 100) {
			super.setBasicPoint(3);
		} else if (point >= 60) {
			super.setBasicPoint(2);
		} else if (point >= 30) {
			super.setBasicPoint(1);
		} else if (point == 0) {
			super.setBasicPoint(0);
		}

	}

	// 포인트 점수로 나뉜 등급에 따라 출력되는 출력문
	@Override
	public void level() {
		switch (super.getBasicPoint()) {
		case 0: {
			System.out.println("초급 지식을 공부하고 오세요 ");
		}
			break;
		case 1: {
			System.out.println("초급 지식을 어느정도 습득했습니다. ");
		}
			break;
		case 2: {
			System.out.println("초급 지식 마스터에 근접했습니다.");
		}
			break;
		case 3: {
			System.out.println("초급 지식을 마스터 했습니다.");
		}
			break;
		}

	}

}
