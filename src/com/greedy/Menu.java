package com.greedy;

import java.util.Scanner;

public class Menu {

	private String name;
	Scanner sc = new Scanner(System.in);
	Question basic = new Basic();
	Question normal = new Normal();
	Question hard = new Hard();

	// 메인 메뉴
	public void menu() {
		System.out.println("안녕하세요 코딩세계에 입문하신 것을 환영 합니다. \n입문 기념으로 케릭터를 하나 드리겠습니다.");

		System.out.print("이 케릭터의 이름은 뭔가요? : ");
		name = sc.next();
		System.out.println();
		System.out.println("당신의 코딩 지식으로 " + name + "을(를) 키울 수 있습니다.");
		System.out.println("문제를 열심히 풀면서 " + name + "을(를) 키워보세요.");

		while (true) {
			System.out.println();
			System.out.println("Menu");
			System.out.println("=======================================");
			System.out.println("1.초급  2.중급  3.고급  4." + name + "의 상태 5.졸업하기");
			System.out.println("=======================================");
			int menu = sc.nextInt();

			// 메뉴선택에 따른 실행 메소드
			switch (menu) {
			case 1:
				basic.question();
				break;
			case 2:
				normal.question();
				break;
			case 3:
				hard.question();
				break;
			case 4:
				stats();
				break;
			case 5:
				System.out.println("캐릭터가 졸업을 합니다.");
				stats();
				return;
			default:
				System.out.println("잘못된 번호를 입력하였습니다. 다시 입력해주세요");
				break;
			}

		}

	}

	// 캐릭터의 상태
	public void stats() {
		System.out.println(name + "님의 지식 수준은 ");
		basic.level();
		normal.level();
		hard.level();
		System.out.println("");
	}

}
