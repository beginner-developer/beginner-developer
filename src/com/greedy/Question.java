package com.greedy;

public abstract class Question {

	private int basicPoint;
	private int normalPoint;
	private int hardPoint;

	public abstract void answer();

	public abstract void question();

	public abstract void point();

	public abstract void level();

	//getters / setters
	public int getBasicPoint() {
		return basicPoint;
	}

	public void setBasicPoint(int basicPoint) {
		this.basicPoint = basicPoint;
	}

	public int getNormalPoint() {
		return normalPoint;
	}

	public void setNormalPoint(int normalPoint) {
		this.normalPoint = normalPoint;
	}

	public int getHardPoint() {
		return hardPoint;
	}

	public void setHardPoint(int hardPoint) {
		this.hardPoint = hardPoint;
	}

}
